#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 18:27:11 2019

@author: harrison

By replacing the 1st digit of the 2-digit number *3, it turns out that six
of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit,
this 5-digit number is the first example having seven primes among the ten
generated numbers, yielding the family:
    56003, 56113, 56333, 56443, 56663, 56773, and 56993.
Consequently 56003, being the first member of this family, is the smallest
prime with this property.

Find the smallest prime which, by replacing part of the number
(not necessarily adjacent digits) with the same digit, is part of an eight
prime value family.
"""

import tools

class PrimeFamily:
    def __init__(self, initial_member):
        self.members = [initial_member]
        self.initial_member = initial_member
        self.size = 1
        
    def find_largest_family(self):
        str_prime = str(self.initial_member)
        zeroes_family = self._replace_digits('0', str_prime)
        ones_family = self._replace_digits('1', str_prime)
                    
        if len(ones_family) < len(zeroes_family):
            self.members = zeroes_family
        else:
            self.members = ones_family
            
        self.size = len(self.members)
        
    def _replace_digits(self, digit, str_prime, missed_count_break=3):
        checked_part = str_prime[:-1]
        family = [str_prime]
        if checked_part.count(digit) > 1:
            missed_count = 0
            for new_digit in '0123456789'.replace(digit, ''):
                new = str_prime.replace(digit, new_digit)
                if tools.check_prime(int(new)) and not new[0] == '0':
                    family.append(new)
                else: missed_count += 1
                
                if missed_count == missed_count_break: break
        
        return family

def main():
    for prime in tools.primes_generator():
        family = PrimeFamily(prime)
        family.find_largest_family()
        if family.size == 8:
            print(family.size)
            print(family.members)
            return family.members[0]
            

if __name__ == '__main__':
    print(main())