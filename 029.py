#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 23:17:12 2018

@author: harrison
"""

def main():
    results = set()
    for a in range(2, 101):
        for b in range(2, 101):
            results.add(a**b)
    
    return len(results)

if __name__ == '__main__':
    print(main())