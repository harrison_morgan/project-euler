from math import sqrt

def doublecheck(prime):
    if prime == 2:
        print("IS TWO")
        return True
    p = True
    for i in range(3, int(prime / 2), 2):
        if prime % i == 0:
            p = False
            break
    if p is False:
        print("FALSE ALARM, FALSE ALARM!!", str(prime))
    return p

max_number = 2000000
primes = [2]
for n in range(3, max_number, 2):
    prime = True
    for i in range(3, int(sqrt(n)) + 1, 2):
        if n % i == 0:
            prime = False
    if prime == True:
        primes.append(n)

total = 0
print(sum(primes))
print("Double check time!")
print(len(primes))
bleh = 0
for prime in primes:
    if doublecheck(prime) is True:
        total += prime
    if bleh > 1000:
        print(prime)
        bleh = 0
    bleh += 1

print(total)
