#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 22:20:43 2018

@author: harrison

The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
"""

def main():
    from fractions import Fraction
    fractions_list = []
    for numerator in range(11, 100):
        for denominator in range(numerator + 1, 100):
            fraction_str = f'{numerator}/{denominator}'
            fraction = Fraction(fraction_str)
            if (not fraction.numerator == numerator) and ("0" not in fraction_str): fractions_list.append((fraction, fraction_str))
    
    answer_list = []
    for fraction, fraction_str in fractions_list:
        if str(fraction) == fraction_str: continue
        if len(str(fraction)) > 3: continue
        numerator, denominator = fraction_str.split('/')
        if int(numerator) == 11 or int(numerator) % 11 == 0: continue
        if int(numerator) / int(denominator) != .5: continue
        if not any([a in set(denominator) for a in set(numerator)]): continue
        reduced_numerator = str(fraction.numerator)
        reduced_denominator = str(fraction.denominator)
        if set(reduced_numerator) - set(reduced_denominator) != set(numerator) - set(denominator): continue
        answer_list.append(fraction_str)
    
    return answer_list

def main_2():
    from fractions import Fraction
    def one_digit_matches(numerator, denominator):
        matches = 0
        match = None
        for digit in str(numerator):
            if digit in str(denominator):
                matches += 1
                match = digit
            
        if matches == 1: return True, match
        else: return False, match
        
    def set_to_int(set):
        a = ''.join(list(set))
        if a == '': return 0
        return int(a)
        
    fractions_list = []
    for numerator in range(11, 100):
        for denominator in range(numerator + 1, 100):
            digit_matches, match = one_digit_matches(numerator, denominator)
            if not digit_matches: continue
            if '0' in str(numerator): continue
            new_numerator = set_to_int(set(str(numerator)) - set(match))
            new_denominator = set_to_int(set(str(denominator)) - set(match))
            if new_denominator == 0: continue
            if Fraction(int(numerator), int(denominator)) == Fraction(new_numerator, new_denominator): fractions_list.append(Fraction(int(numerator), int(denominator)))
    return fractions_list[0] * fractions_list[1] * fractions_list[2] * fractions_list[3]
            
if __name__ == '__main__':
    print(main_2())