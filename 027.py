#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 22:29:18 2018

@author: harrison
"""

from tools import check_prime

def main():
    largest_n_primes = 0
    for a in range(-999, 1000):
        for b in range(-1000, 1001):
            n = 0
            while check_prime((n**2) + (a*n) + b):
                n += 1
            if n > largest_n_primes:
                largest_n_primes = n
                answer = a * b
    
    return answer
    
if __name__ == '__main__':
    print(main())