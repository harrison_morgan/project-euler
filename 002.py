total = 0

def fibb(last, current, cache=[]):
    if cache == []:
        cache.append(current)
    new = last + current
    
    if new < 4000000:
        if new % 2 == 0:
            cache.append(new)
        fibb(current, new, cache)
    return cache

for number in fibb(1, 2):
    total += number

print(total)


