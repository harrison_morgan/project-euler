#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 13:07:25 2019

@author: harrison

The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1);
so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its
alphabetical position and adding these values we form a word value.
For example, the word value for SKY is 19 + 11 + 25 = 55 = t10.
If the word value is a triangle number then we shall call the word a
triangle word.

Using words.txt (right click and 'Save Link/Target As...'),
a 16K text file containing nearly two-thousand common English words,
how many are triangle words?
"""

import itertools

def generate_triangle_numbers():
    for n in itertools.count(1):
        yield int((n / 2) * (n + 1))

def generate_words():
    with open('042_words.txt') as words_file:
        words = words_file.read()
        words = words.strip('"')
        words_list = words.split('","')
    for word in words_list:
        yield word

def main():
    triangle_number_generator = generate_triangle_numbers()
    triangle_numbers = [next(triangle_number_generator)]
    number_of_triangle_words = 0
    for word in generate_words():
        
        word_number = 0
        for character in word:
            word_number += ord(character) - 64
            
        while triangle_numbers[-1] < word_number:
            triangle_numbers.append(next(triangle_number_generator))
        
        if word_number in triangle_numbers:
            number_of_triangle_words += 1
            
    return number_of_triangle_words

if __name__ == '__main__':
    print(main())