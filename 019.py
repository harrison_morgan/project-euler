#Jan 1900 was a Monday.

#Thirty days has September,
#April, June and November.
#All the rest have thirty-one,
#Saving February alone,
#Which has twenty-eight, rain or shine.
#And on leap years, twenty-nine.

#A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

#How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

class Year:
    def __init__(self, year):
        self.year = year
        
        self.century = False
        if str(year)[-2:] == "00":
            self.century = True
        
        self.leapyear = False
        if self.century:
            if year % 400 == 0:
                self.leapyear = True
        else:
            if year % 4 == 0:
                self.leapyear = True
        
    def get_day_of_week(self, month, day):
        a = (14 - month) / 12
        y = self.year - a
        m = month + 12*a - 2
        return (day + y + y/4 - y/100 + y/400 + (31*m)/12) % 7

def old_way():
    weeknames = {0: "Sunday", 1: "Monday", 2: "Tuesday", 3: "Wednesday",
                 4: "Thursday", 5: "Friday", 6: "Saturday"}
    year = Year(2012)
    total_first_sundays = 0
    for year in range(1901, 2001):
        year = Year(year)
        for month in range(1, 13):
            if year.get_day_of_week(month, 1) == 0:
                total_first_sundays += 1
    
    print("Answer:", total_first_sundays)

def new_way():
    import datetime
    total = 0
    for year in range(1901, 2001):
        for month in range(1, 13):
            if datetime.date(year, month, 1).weekday() == 6:
                total += 1
    print("Answer:", total)

if __name__ == "__main__":
    new_way()