#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 17:10:44 2019

@author: harrison

The prime 41, can be written as the sum of six consecutive primes:

41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below
one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime,
contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most
consecutive primes?
"""

import tools

def main():
    primes = []
    target = 1000000
    most_consecutive_primes = 0
    answer = 0
    primes_generator = tools.primes_generator()
    
    for prime_number in primes_generator:
        primes.append(prime_number)
        summation_of_primes = sum(primes)
        if summation_of_primes > target:
            deleted_prime = primes.pop()
            while sum(primes) + deleted_prime > target:
                del primes[0]
                if (tools.check_prime(sum(primes))
                        and len(primes) > most_consecutive_primes):
                    most_consecutive_primes = len(primes)
                    answer = sum(primes)
            break
        
        if tools.check_prime(summation_of_primes):
            most_consecutive_primes = len(primes)
            answer = summation_of_primes

    while True:
        if primes[-1] > target:
            break
        while sum(primes) > target:
            del primes[0]
        
        if sum(primes) == 953: print('953:', primes)
        if (tools.check_prime(sum(primes))
                and len(primes) > most_consecutive_primes):
            
            most_consecutive_primes = len(primes)
            answer = sum(primes)
        
        primes.append(next(primes_generator))
    
    return answer

if __name__ == '__main__':
    print(main())