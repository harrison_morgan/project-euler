import time

def mine():
    def is_abundant(n):
        # A number is abundant if the sum of its divisors is greater than the number itself
        # I think there're faster ways to do this, but I didn't understand any of them
        # So I left it alone
        divisors = 0
    
        for i in range(2, int(n/2 + 1)):
            if n % i == 0:
                divisors += i
                
        return divisors > n
    
    start = time.time()
    
    # Any number above 28,123 is known to be the sum of two abundant numbers
    limit = 28124
    numbers = set(range(1, limit))
    abundant_numbers = [n for n in numbers if is_abundant(n)]
    abundant_numbers2 = abundant_numbers[:]
    sums = set()
    
    for abundant_number in abundant_numbers:
        for another_abundant_number in abundant_numbers2:
            sum_ = abundant_number + another_abundant_number
            if sum_ < limit:
                sums.add(sum_)
            else:
                break
        # I'm particularly proud about discovering this
        # I didn't see it in any of the other solutions
        # And it even significantly improves the already-fast forum_post() solution
        abundant_numbers2.remove(abundant_number)
    
    total = sum(numbers - sums)
    
    print("Time:", time.time() - start)
    print("Answer:", total)

def forum_post():
    # From a post on page 2 of Problem #23's forum
    # Kind of ugly, but very fast
    # I think its speed lies in the abun function, if only I could decipher it
    def abun(N):
        Q = dict.fromkeys(range(1,N+1), 0) 
        for q in Q:
            for k in [q*n for n in range(1,int(N/q+1)) ]:
                if q!=k: Q[k] += q
        return [ q  for q in Q if Q[q]>q]
    
    start = time.time()
    
    # Note: 20,161 is the actual limit; I used 28,123 because Project Euler gives that one
    # The difference is because 20,161 is from experiment; 28,123 is the theoretical limit
    N = 20161; A = abun(N); possible = set() 
    for a in A:
        for b in A:
            if a+b < N: possible.add( a+b )
            else: break
            
    print(sum([p for p in range(N) if p not in possible]))
    print("Time:", time.time() - start)
    
if __name__ == '__main__':
    mine()
    forum_post()
