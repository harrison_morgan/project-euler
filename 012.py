import time, math

def triangle_numbers():
    count = 2
    number = 1
    while True:
        yield number
        number += count
        count += 1
        
for i in triangle_numbers():
    divisors = 0
    divisor = 1
    while divisor < math.sqrt(i):
        if i % divisor == 0:
            divisors += 2
        divisor += 1
    
    print(i, divisors)
    if divisors > 500:
        break

print(time.clock())
