#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 13:52:46 2018

@author: harrison

Take the number 192 and multiply it by each of 1, 2, and 3:

    192 × 1 = 192
    192 × 2 = 384
    192 × 3 = 576

By concatenating each product we get the 1 to 9 pandigital, 192384576. 
We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by
1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated
product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the
concatenated product of an integer with (1,2, ... , n) where n > 1?
"""

from tools import is_pandigital

def main():
    largest = 0
    for x in range(2, 10000):
        concatenated = str(x)
        for y in range(2, 7):
            new = x * y
            concatenated += str(new)
            if len(concatenated) > 9:
                break
            if is_pandigital(concatenated):
                if int(concatenated) > largest:
                    largest = int(concatenated)
    
    return(largest)
            

if __name__ == '__main__':
    print(main())