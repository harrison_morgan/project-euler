#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 11:15:06 2018

@author: harrison

In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
"""

import itertools

def main_3():
    def get_coins_for(amount, coin=200, coins_list=[200, 100, 50, 20, 10, 5, 2, 1]):
        number_of_ways = 0
        index = coins_list.index(coin)
        last_coin = False
        if index + 1 == len(coins_list):
            last_coin = True

        while amount > 0:
            if not last_coin: number_of_ways += get_coins_for(amount, coins_list[index + 1])
            amount -= coin
            if amount == 0: number_of_ways += 1
        
        return number_of_ways
    
    return get_coins_for(200)
            
            

def main_2():
    coeffs = [1, 2, 5, 10, 20, 50, 100, 200]
    from numpy.polynomial import Polynomial as P
    p = P(coeffs)
    result = p(200)
    return result

def main():
    coins = [1, 2, 5, 10, 20, 50, 100, 200]
    max_each = []
    
    for coin in coins:
        total = 0
        number_of_coins = 0
        while total < 200:
            total += coin
            number_of_coins += 1
        max_each.append(number_of_coins)
        
    total_coins = []
    for index, coin in enumerate(coins):
        total_coins += [coin] * max_each[index]
    
    combinator = itertools.combinations_with_replacement(total_coins)
    count = 0
    for combination in combinator:
        if sum(combination) == 200:
            count += 1
            
    return count
    
if __name__ == '__main__':
    print(main_3())