import math, sys, time

def find_next_prime(n):
    if n == 2:
        print("N IS TWO")
        return 3
    if n % 2 == 0:
        print("N IS DIVISIBLE BY TWO")
        n -= 1
    
    n += 2

    while True:
        v = int(math.sqrt(n + 1))
        found = False
        for i in range(2, v):
            if (n % i == 0):
#                print "FOUND: n: " + str(n) + " i: " + str(i) + " v: " + str(v)
                found = True
                break
        if found == False:
            return n
        n += 2

def is_not_prime(n):
    v = int(math.sqrt(n)) + 1
    for i in range(3, v):
        if n % i == 0:
            return True
    return False

check = 600851475143
factors = []
prime = 3

while True:
    if check % prime == 0:
        factors.append(prime)
        print(check / prime)
        check /= prime
        print(check)
        print(prime)
        print("---------")
        
    elif is_not_prime(check):
        prime = find_next_prime(prime)
        
    else:
        factors.append(check)
        break

print(factors)
factors.sort()
print(factors[-1])

double_check = 1
last = None
for i in factors:
    double_check *= i

print(double_check)
    
