#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 08:40:50 2019

@author: harrison

An irrational decimal fraction is created by concatenating the positive integers:

0.12345678910 1 112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the
following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
"""

def old_main():
    index = 0
    number = 0
    goal_indexes = [1000000, 100000, 10000, 1000, 100, 10, 1]
    digits = []
    while index < 1000000:
        number += 1
        str_number = str(number)
        index += len(str_number)
        if index >= goal_indexes[-1]:
            goal_index = goal_indexes.pop()
            digits.append(str_number[index - goal_index - 1])
    
    print(digits)
    answer = 1
    for digit in digits:
        answer *= int(digit)
        
    return answer
            
def main():
    numbers = ""
    next_int = 1
    while len(numbers) < 1000000:
        numbers += str(next_int)
        next_int += 1
    
    n = numbers
    print(n[0], n[9], n[100], n[1000], n[10000], n[100000], n[1000000])
    return int(n[0]) * int(n[9]) * int(n[99]) * int(n[999]) * int(n[9999]) * int(n[99999]) * int(n[999999])
        

if __name__ == '__main__':
    print(main())