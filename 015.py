from math import factorial

def confusing(x, y):
    return factorial(x) / (factorial(y) * factorial(x - y))

print(confusing(40, 20))
