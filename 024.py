import time
import itertools

def mine():
    start = time.time()
    sequence = "0123456789"
    count = 1
    for permutation in itertools.permutations(sequence):
        if count == 1000000:
            answer = "".join(permutation)
            break
        count += 1
    
    print("Time:", time.time() - start)
    print("Answer:", answer)
    
if __name__ == "__main__":
    mine()