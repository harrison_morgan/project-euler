#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 19:49:10 2018

@author: harrison

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
"""

def main():
    return sum({a * b for a in range(2, 51) for b in range(51, 2000) if len((str(a) + str(b) + str(a * b))) == 9 and len((str(a) + str(b) + str(a * b))) == len(set((str(a) + str(b) + str(a * b)))) and "0" not in (str(a) + str(b) + str(a * b))})
    answer = set()
    for a in range(2,  60): #These ranges found on https://blog.dreamshire.com/project-euler-32-solution/
        start = 1234 if a < 10 else 123 
        for b in range(start, 10000//a):
    #for a in range(2, 51):
    #     for b in range(51, 501):
             if len((str(a) + str(b) + str(a * b))) == 9:
                 if len((str(a) + str(b) + str(a * b))) == len(set((str(a) + str(b) + str(a * b)))):
                     if "0" not in str(a) + str(b) + str(a * b):
                         print(a, b, a*b)
                         answer.add(a*b)
    return sum(answer)
                 
if __name__ == '__main__':
    print(main())