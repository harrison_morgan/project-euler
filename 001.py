import time

slow_start = time.time()
total = 0
for i in range(1, 1000):
    if i % 3 == 0 or i % 5 == 0:
        total += i
print(total)
print(time.time() - slow_start)
print("~-~")

def f(k, n):
#    result = []
#    for i in range(1, n / k):
#        result.append(k * ((n * (n + 1)) / 2))
#    return result
    return k * ((n * (n + 1)) / 2)

fast_start = time.time()
print(f(3, 1000))
print(time.time() - fast_start)
