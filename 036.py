#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 13:43:05 2018

@author: harrison

The decimal number, 585 = 1001001001 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)
"""

def main():
    answer = 0
    for i in range(1000000):
        
        test_10 = str(i)
        
        if test_10 == test_10[::-1]:
            test_2 = bin(i)[2:]
            if test_2 == test_2[::-1]: answer += i
    
    return answer

if __name__ == '__main__':
    print(main())