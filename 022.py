names = sorted(open("022names").read().replace('"', '').split(","))

alpha_to_num = {}
letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
value = 1
for letter in letters:
    alpha_to_num[letter] = value
    value += 1

position = 1
total = 0
for name in names:
    temp = 0
    for letter in name:
        temp += alpha_to_num[letter]
    total += temp * position
    position += 1

print("Answer:", total)
