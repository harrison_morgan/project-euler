#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 25 02:01:26 2019

@author: harrison

The arithmetic sequence, 1487, 4817, 8147, in which each of the terms
increases by 3330, is unusual in two ways: (i) each of the three terms
are prime, and, (ii) each of the 4-digit numbers are permutations of
one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes,
exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms
in this sequence?
"""

import tools

def main():
    primes_list = []
    for prime in tools.primes_generator():
        if prime > 9999:
            break
        if prime > 999:
            primes_list.append(prime)
            
    for prime in primes_list:
        if prime == 1487:
            continue
        digits = sorted(str(prime))
        for prime2 in primes_list:
            digits2 = sorted(str(prime2))
            if digits == digits2 and prime != prime2:
                for prime3 in primes_list:
                    digits3 = sorted(str(prime3))
                    if digits2 == digits3:
                        if prime3 - prime2 == prime2 - prime:
                            return str(prime) + str(prime2) + str(prime3)
                    

if __name__ == '__main__':
    print(main())