#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 22:01:54 2018

@author: harrison
"""

from tools import fibonacci_generator

def main():
    fib_gen = fibonacci_generator()
    index = 1
    while True:
        fib = next(fib_gen)
        if len(str(fib)) == 1000:
            print('Answer:', index)
            break
        index += 1

if __name__ == '__main__':
    main()