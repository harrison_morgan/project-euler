#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 12:18:19 2018

@author: harrison

The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
"""

def main():
    from tools import primes_generator
    from collections import deque
    prime_gen = primes_generator()
    primes_list = []
    prime = 0
    while prime < 1000000:
        prime = next(prime_gen)
        primes_list.append(prime)
    total = 0
    for prime in primes_list:
        digits = deque(str(prime))
        for _ in range(len(digits) - 1):
            digits.rotate(1)
            if int(''.join(digits)) not in primes_list: break
        else:
            total += 1
    
    return total
            
        

if __name__ == '__main__':
    print(main())