number = 2520

while True:
    all_divisible = True
    for i in range(11, 21):
        if number % i != 0:
            all_divisible = False
            break
    if all_divisible is True:
        print(number)
        break
    else:
        number += 2520
