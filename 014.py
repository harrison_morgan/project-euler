import time

number = 2
greatest_length = 0
greatest_number = 0
while number <= 1000000:
    chain_length = 1
    chain = number
    while chain > 1:
        if chain % 2 == 0:
            chain /= 2
            chain_length += 1
        else:
            chain *= 3
            chain += 1
            chain_length += 1
    
    if chain_length > greatest_length:
        greatest_number = number
        greatest_length = chain_length
    
    number += 1

print("Answer:", greatest_number)
print(time.clock())
