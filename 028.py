#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 22:49:44 2018

@author: harrison
"""

def main():
    end_of_spiral = 1001 * 1001
    number = 1
    multiplier = 2
    total = 1
    while number < end_of_spiral:
        for _ in range(4):
            number += multiplier
            total += number
        multiplier += 2
        
    return total

if __name__ == '__main__':
    print(main())