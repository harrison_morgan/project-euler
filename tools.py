#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 20:57:00 2018

@author: harrison
"""
import math
import itertools

def check_prime(number):
    if number < 2: return False
    if number == 3: return True
    if number == 4: return False
    if number == 6: return False
    if number == 8: return False
    
    for i in range(2, int(math.sqrt(number)) + 1):
            if number % i == 0:
                break
    else:
        return True
    
    return False
    
def primes_generator(starting_number=2):
    if starting_number == 2:
        yield 2
        starting_number += 1
    test = starting_number
    if test % 2 == 0:
        test += 1
    while True:
        if check_prime(test): yield test
        test += 2
        
class PrimesList(list):
    def __iter__(self):
        iterator = super().__iter__()
        for item in iterator:
            yield item
        
        starting_number = 2
        if len(self):
            starting_number = self[-1] + 1
        
        for prime in primes_generator(starting_number):
            self.append(prime)
            yield prime
        
def prime_factors(x):
    factors = []
    
    for i in primes_list:
        if i*i > x:
            if x > 1: factors.append(x)
            break
        
        while x % i == 0:
            factors.append(i)
            x /= i
    
    return factors

class Primes:
    def __init__(self, max_cache=10000):
        self.max_cache = max_cache
        
        #Primes are cached by their index; 2 = the 0th prime, 3 = 1st, etc.
        self.cache = {}
        self.beginning_cache_state = 0
        
        self.generator = primes_generator
        self.current_generator = self.generator()
        self.generator_state = 0
        
        self.generators = {}
    
    @staticmethod
    def is_prime(number):
        return check_prime(number)
    
    def generate_primes(self, amount):
        primes = []
        for _ in range(amount + 1):
            prime = next(self.current_generator)
            self.cache[self.generator_state] = prime
            self.generator_state += 1
            
            if len(self.cache) > self.max_cache:
                del self.cache[self.beginning_cache_state]
                self.beginning_cache_state = self.cache.keys()[0]
            
            primes.append(prime)
        
        return primes
    
    def get_closest_index_below(self, number):
        pass
    
    def get_primes_between(self, start, stop=None):
        if stop == None:
            if stop < 0: raise IndexError('No negative primes')
            stop = start
            start = 2
        if start < 0: raise IndexError('No negative primes')
        if stop < start: raise IndexError('Stop cannot be less than start')
        
        start_index = -1
        stop_index = -1
        for index, prime in self.cache.items():
            if prime == start: start_index = index
            elif prime == stop: stop_index = index
            
        if start_index == -1:
            new_generators = {}
            closest_generator = None
            closest_state = None
            closest_prime = None
            
            for generator_state, generator in self.generator.items():
                prime = next(generator)
                
                if (start - prime) < closest_prime:
                    closest_generator = generator
                    closest_state = generator_state
                    closest_prime = start - prime
                
                new_generators[generator_state + 1] = generator
            
            if closest_prime == 0 and stop_index != -1:
                pass
                
        
        #TODO: list all primes between two numbers, not based on index
    
    def _switch_to_nearest_generator(self, state):
        self.generators = dict(sorted(self.generators.items()))
        current_state = self.generator_state
        prev_key = None
        cache_state = 0
        for key in sorted(self.cache.keys()):
            if key > state:
                if prev_key:
                    cache_state = prev_key
                    break
        
        prev_key = None
        generators_state = 0
        for key in sorted(self.generators.keys()):
            if key > state:
                if prev_key:
                    generators_state = prev_key
                    break
        
        closest = max((current_state, cache_state, generators_state))
        if closest == current_state: return 1
        if closest == generators_state:
            self.generators[self.generator_state] = self.current_generator
            self.current_generator = self.generators.pop(generators_state)
            self.generator_state = generators_state
            return 2
        if closest == cache_state:
            self.generators[self.generator_state] = self.current_generator
            self.current_generator = self.generator(self.cache[cache_state])
            self.generator_state = cache_state
            return 3
        
        self.generators[self.generator_state] = self.current_generator
        self.current_generator = self.generator()
        self.generator_state = 0
        return 4
        
    def __getitem__(self, key):
        if isinstance(key, int):
            if key < 0: raise IndexError(f'The key {key} is out of range.')
            if key in self.cache: return self.cache[key]
            if key == 0: return 2
            self._switch_to_nearest_generator(key)
            return self.generate_primes(key - self.generator_state)[-1]
        
        elif isinstance(key, slice):
            if key.start < 0 or key.stop < 0: raise IndexError(f'The key {key.start} or {key.stop} is out of range.')
            keys = range(key.start, key.stop)
            section = []
            for i in keys:
                section.append(self[i])
                
            return section
            
        else:
            raise TypeError(f'Invalid type: {key}')

def fibonacci_generator():
    number1 = 1
    yield number1
    number2 = 1
    yield number2
    while True:
        new_number = number1 + number2
        yield new_number
        number2 = number1
        number1 = new_number
        
def is_pandigital(number_to_test, min_digit=1, max_digit=9):
    number_to_test = "".join(sorted(str(number_to_test)))
    digits = "".join([str(x) for x in range(min_digit, max_digit + 1)])
    if number_to_test == digits: return True
    else: return False
    
def make_pandigital_iterator(start=1, end=9):
    digits = [str(x) for x in range(start, end + 1)]
    
    def generator(permutations):
        for permutation in permutations:
            yield int(''.join(permutation))
    
    return generator(itertools.permutations(digits))

class FigurateNumbers:
    @staticmethod
    def generate_triangle_numbers():
        n = 1
        while True:
            yield int((n*(n+1)) / 2)
            n += 1
            
    @staticmethod
    def is_triangle(x):
        result = (math.sqrt(8*x+1) - 1) / 2
        return result.is_integer
    
    @staticmethod
    def generate_pentagonal_numbers():
        n = 1
        while True:
            yield int((n*((3*n)-1)) / 2)
            n += 1
    
    @staticmethod
    def is_pentagonal(x):
        result = (math.sqrt(24*x+1) + 1) / 6
        return result.is_integer()
    
    @staticmethod
    def generate_hexagonal_numbers():
        n = 1
        while True:
            yield n * (2*n-1)
            n += 1
    
    @staticmethod        
    def is_hexagonal(x):
        result = (math.sqrt(8*x+1) + 1) / 4
        return result.is_integer()
    
    
primes_list = PrimesList()
        
if __name__ == '__main__':
    p = Primes()
    print("p[0]", p[0])
    print("p[1]", p[1])
    print("p[1]", p[1])
    print("p[0:5]", p[0:5])