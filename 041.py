#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 19:10:21 2019

@author: harrison

We shall say that an n-digit number is pandigital if it makes use of
all the digits 1 to n exactly once.
For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
"""

import tools

def main():
    largest = 0
    for n in range(2, 10):
        iterator = tools.make_pandigital_iterator(end=n)
        for i in iterator:
            if i > largest and tools.check_prime(i):
                largest = i
    
    return largest

if __name__ == '__main__':
    print(main())