#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 20 21:15:04 2019

@author: harrison

The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors
each. What is the first of these numbers?
"""

import tools

def distinct_factors(factors):
    return len(set(factors))

def test_consecutive_integers(start, end):
    for i in range(end, start, -1):
        number_of_factors = distinct_factors(tools.prime_factors(i))
        if number_of_factors != 4:
            return i + 1
    else:
        return i
            
def main():
    index = 2*3*5*7
    while True:
        result = test_consecutive_integers(index - 1, index + 3)
        if result == index:
            return result
        index = result

if __name__ == '__main__':
    print(main())