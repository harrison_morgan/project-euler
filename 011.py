import time

class Grid:
    def __init__(self, width, height, block_size=1, iterable=False):
        self.width = width
        self.height = height
        self.block_size = block_size
        
        self.grid = {}
        self.updated = {}
        self.layers = {}
        
        self.debug = False
        self.iterable = iterable
        self.itercount = 0
        if iterable:
            self.itermax = len(iterable)
        else:
            self.itermax = -1
        
        self.reset_grid()
    
    def __repr__(self):
        return str(self.grid)
        
    def reset_grid(self):
        block_size = self.block_size
        
        if self.debug: print("reset_grid start: block_size " + str(block_size) + "; width " + str(self.width) + "; height " + str(self.height))
        
        total_used_x, total_used_y, xblocks, yblocks = 0,0,0,0
        remaining_x = self.width
        remaining_y = self.height
        
        while remaining_x >= block_size or remaining_y >= block_size:
            self._add(xblocks, yblocks, total_used_x, total_used_y)
            total_used_x += block_size
            xblocks += 1
            if remaining_x < block_size:
                xblocks, total_used_x = 0,0
                remaining_x = self.width
                yblocks += 1
                remaining_y -= block_size
                total_used_y += block_size
            remaining_x -= block_size
        
        self.max_coords = (xblocks - 1, yblocks)
        if self.debug: print("reset_grid finish: max " + str(self.max_coords))
        
    def _add(self, xblock, yblock, x, y):
        if self.debug: print("_add: xblock " + str(xblock) + "; yblock " + str(yblock), "; x " + str(x) + "; y " + str(y))
        key = (xblock, yblock)
        if self.iterable and self.itercount < self.itermax:
            options = [(x, y, self.block_size, self.block_size), self.iterable[self.itercount]]
            self.grid[key] = options
            self.updated[key] = options
            self.itercount += 1
        else:
            options = [(x, y, self.block_size, self.block_size), 0]
            self.grid[key] = options
            self.updated[key] = options
        
    def get_block(self, key, delete_updated=True):
        if self.debug: print("get_block start key: " + str(key))
        assert key in self.grid, "get_block couldn't find " + str(key)
        if delete_updated:
            if key in self.updated: del self.updated[key]
        return self.grid[key] #Should return a list with the options
    
    def is_valid_block(self, key):
        if key in self.grid:
            return True
        return False


numbers = \
"""08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48"""

numbers = [int(x) for x in numbers.split()]

grid = Grid(20, 20, iterable=numbers)
x = 0
y = 0
largest = 0
while True:
    if x == 20:
        x = 0
        y += 1
    if y == 20:
        print(largest)
        break
    
    v_down = [(x, y+1), (x, y+2), (x, y+3)]
    v_up = [(x, y-1), (x, y-2), (x, y-3)]
    h_left = [(x-1, y), (x-2, y), (x-3, y)]
    h_right = [(x+1, y), (x+2, y), (x+3, y)]
    d_down_l = [(x-1, y+1), (x-2, y+2), (x-3, y+3)]
    d_down_r = [(x+1, y+1), (x+2, y+2), (x+3, y+3)]
    d_up_l = [(x-1, y-1), (x-2, y-2), (x-3, y-3)]
    d_up_r = [(x+1, y-1), (x+2, y-2), (x+3, y-3)]
    
    directions = (v_down, v_up, h_left, h_right, d_down_l, d_down_r, d_up_l, d_up_r)
    for direction in directions:
        product = 1
        for coords in direction:
            if not grid.is_valid_block(coords):
                break
        else:
            multiplicands = [grid.get_block((x, y))[1]]
            for coords in direction:
                multiplicands.append(grid.get_block(coords)[1])
            for i in multiplicands:
                product *= i
        if product > largest:
            largest = product
            print(multiplicands)
    
    x += 1

print(time.clock())
