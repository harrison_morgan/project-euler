import math

def double_check(prime):
    p = True
    for i in range(2, prime):
        if prime % i == 0:
            p = False
    return p

primes = [2]
dc = [2]
m = 3
while True:
    prime = True
    for i in range(2, int(math.sqrt(m + 1))):
        if m % i == 0:
            prime = False
    if prime == True:
        primes.append(m)
    if len(primes) == 10066:
        print(primes[-1])
        break
    m += 2

for prime in primes[1:]:
    if double_check(prime):
        dc.append(prime)
    else:
        print(prime)

print(len(dc))
print(dc[-1])


