"""
@author: harrison

It can be seen that the number, 125874, and its double, 251748, contain
exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x,
contain the same digits.
"""

import itertools

def compare_digits(base: int, multiplier: int):
    multiplied = base * multiplier
    base_str = sorted(str(base))
    multiplied_str = sorted(str(multiplied))
    
    if base_str == multiplied_str:
        return True, False
    
    if len(base_str) != len(multiplied_str):
        return False, True
    
    return False, False

def main():
    start = 1
    while True:
        skip = False
        for base in itertools.count(start):
            answer = True
            for multiplier in range(2, 7):
                is_same, skip = compare_digits(base, multiplier)
                if not is_same: answer = False
                if skip: break
            if answer: return base
            if skip:
            
                start *= 10
                break
        
        

if __name__ == '__main__':
    print(main())
