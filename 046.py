#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 20 20:50:45 2019

@author: harrison

It was proposed by Christian Goldbach that every odd composite number
can be written as the sum of a prime and twice a square.

9 = 7 + 2×1^2
15 = 7 + 2×2^2
21 = 3 + 2×3^2
25 = 7 + 2×3^2
27 = 19 + 2×2^2
33 = 31 + 2×1^2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a
prime and twice a square?
"""

import tools
import itertools

def main():
    primes = [2]
    for i in itertools.count(3, 2):
        if tools.check_prime(i):
            primes.append(i)
            continue
        
        for prime in primes:
            square = 1
            test = 0
            while test < i:
                test = prime + 2 * pow(square, 2)
                square += 1
            if test == i:
                break
        else:
            return i
                

if __name__ == '__main__':
    print(main())