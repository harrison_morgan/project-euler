def number_to_words(number):
    number = str(number)
    
    tens = {"10": "ten", "11": "eleven", "12": "twelve", "13": "thirteen",
                "14": "fourteen", "15": "fifteen", "16": "sixteen",
                "17": "seventeen", "18": "eighteen", "19": "nineteen"}
                
    other_tens = {"2": "twenty", "3": "thirty", "4": "forty", "5": "fifty",
                  "6": "sixty", "7": "seventy", "8": "eighty", "9": "ninety"}
                  
    ones = {"1": "one", "2": "two", "3": "three", "4": "four", "5": "five",
                  "6": "six", "7": "seven", "8": "eight", "9": "nine"}
                  
    place_values = {1: "thousand", 2: "million", 3: "billion", 4: "trillion"}
    
    # Check for shorter (1 or 2 digit) numbers
    if len(number) == 1:
        return ones[number]
    elif len(number) == 2:
        if number[0] == "1":
            return tens[number]
        elif number[1] == "0":
            return other_tens[number[0]]
        return other_tens[number[0]] + "-" + ones[number[1]]
    
    ## Break up the number into place-value groups
    # Reverse to go in order
    number = number[::-1]
    count = -1
    value_count = 0
    place_valued = []
    for n in number:
        if value_count == 0:
            place_valued.append("")
            count += 1
        elif value_count == 2:
            place_valued[count] += n
            place_valued[count] = place_valued[count][::-1]
            value_count = 0
            continue
        
        place_valued[count] += n
        value_count += 1
        
    count = 0
    all_words = []
    for group in place_valued:
        check_ones = False
        if group[0] == "0":
            count += 1
            continue
        elif len(group) == 1:
            group_words = ones[group[0]] + " "
        else:
            group_words = ones[group[0]] + " hundred "
        
        if len(group) == 1:
            if count > 0:
                group_words += place_values[count]
            all_words.append(group_words)
            count += 1
            continue
        
        if group[1] == "0":
            check_ones = True
        elif group[1] == "1":
            group_words += "and " + tens[group[1:3]]
        elif group[2] == "0":
            group_words += "and " + other_tens[group[1]]
        else:
            group_words += "and " + other_tens[group[1]] + "-" + ones[group[2]]
        
        if check_ones == True and group[2] != "0":
            group_words += "and " +  ones[group[2]]
        
        if count > 0:
            group_words += " " + place_values[count]
        
        all_words.append(group_words)
        count += 1
    
    return ", ".join(all_words[::-1])


def count_letters(words):
    new_words = ""
    for character in words:
        if character == " " or character == "-" or character == ",":
            continue
        new_words += character
    
    return len(new_words)

letter_count = 0
for i in range(1, 1001):
    words = number_to_words(i)
    print(words)
    letter_count += count_letters(words)

print("Answer:", letter_count)
