#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 25 01:53:19 2019

@author: harrison

The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
"""

def main():
    cumulative = 0 # Could have used a list comprehension
    for i in range(1, 1001):
        cumulative += i**i
    
    return str(cumulative)[-10:]

if __name__ == '__main__':
    print(main())