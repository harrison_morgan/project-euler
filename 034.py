#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 01:44:06 2018

@author: harrison

145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
"""

def main():
    from math import factorial
    factorials = [factorial(x) for x in range(10)]
    total = 0
    for i in range(3, 10000000):
        digits_to_factorial = sum([factorials[int(x)] for x in str(i)])
        if digits_to_factorial == i: total += i
    
    return total
        
        

if __name__ == '__main__':
    print(main())