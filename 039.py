#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 15:01:46 2019

@author: harrison

If p is the perimeter of a right angle triangle with integral length sides,
{a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?
"""

def is_right_triangle(a, b, c):
    if a**2 + b**2 == c**2: return True
    return False

def main():
    most_number_of_solutions = 0
    best_p = 0
    for p in range(6, 1001, 2):
        number_of_solutions = 0
        for a in range(int(p/6) + 1, int(p/3) + 1):
            for b in range(a, int(p/2) + 1):
                c = p - (a + b)
                if is_right_triangle(a, b, c):
                    number_of_solutions += 1
        if number_of_solutions > most_number_of_solutions:
            most_number_of_solutions = number_of_solutions
            best_p = p
    
    return best_p
        

if __name__ == '__main__':
    print(main())