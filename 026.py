#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 22:08:55 2018

@author: harrison
"""

def main():
    longest_cycle = 0
    for denominator in range(2, 1000):
        seen_remainders = set()
        remainder = 1 % denominator
        while True:
            if remainder == 0: break
            elif remainder in seen_remainders:
                cycle = len(seen_remainders)
                if cycle > longest_cycle:
                    longest_cycle = cycle
                    longest_cycle_denominator = denominator
                break
            
            seen_remainders.add(remainder)
            remainder *= 10
            remainder = remainder % denominator
        
    return longest_cycle_denominator

if __name__ == '__main__':
    print(main())