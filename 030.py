#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 23:22:17 2018

@author: harrison
"""

def main():
    number = 2
    results = []
    while number < 1000000:
        test = 0
        for digit in [int(i) for i in str(number)]:
            test += digit**5
        if test == number: results.append(number)
        number += 1
    
    return sum(results)

if __name__ == '__main__':
    print(main())