#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 12:34:15 2018

@author: harrison

The number 3797 has an interesting property. Being prime itself, it is possible
to continuously remove digits from left to right, and remain prime at each
stage: 3797, 797, 97, and 7. Similarly we can work from right to left:
3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to
right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
"""

from tools import primes_generator, check_prime

def check_truncatable(prime):
    for i in range(1, len(prime)):
        if check_prime(int(prime[i:])) and check_prime(int(prime[:-i])):
            continue
        else:
            break
    else:
        return True

def main():
    gen = primes_generator(9)
    answer_list = []
    while len(answer_list) < 11:
        prime = next(gen)
        if check_truncatable(str(prime)):
            answer_list.append(prime)
    
    return(sum(answer_list))
        
if __name__ == '__main__':
    print(main())