import time
from math import sqrt
#import line_profiler

#profile = line_profiler.LineProfiler()

#@profile
def sum_proper_divisors(n):
    divisors = 1

    for i in range(2, int(sqrt(n))):
        if n % i == 0:
            divisors += i; divisors += n/i
            
    return divisors

def main():
    amicable_numbers = set()
    divisor_sum_list = []
    for x in range(1, 10000):
        a = sum_proper_divisors(x)
        if a != 1:
            divisor_sum_list.append((x, a))
    for i in range(0, len(divisor_sum_list) - 1):
        for x in range(i, len(divisor_sum_list) - 1):
            n1, divisor_sum1 = divisor_sum_list[i]
            n2, divisor_sum2 = divisor_sum_list[x]
            if n1 == divisor_sum2 and n2 == divisor_sum1 and n1 != n2:
                amicable_numbers.add(n1)
                amicable_numbers.add(n2)
                print(n1, n2)

    print("Time:", time.clock())
    print("Answer:", sum(amicable_numbers))
    
def test():
    for i in range(1, 10000):
        sum_proper_divisors(i)
    
main()

#profile.print_stats()
