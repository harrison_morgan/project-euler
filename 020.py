product = 1
for n in range(100, 0, -1):
    product *= n

print("Answer:", sum([int(x) for x in str(product)]))
