import time
start = time.time()
largest = 0

for i in range(100, 1000):
    for k in range(i, 1000):
        result = i * k
        if int(str(result)[::-1]) == result and result > largest:
            largest = result
print(largest)
print(time.time() - start)
